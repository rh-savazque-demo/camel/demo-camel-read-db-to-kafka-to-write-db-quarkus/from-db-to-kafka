package com.redhat.route;

import com.redhat.entity.User;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import java.util.List;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;

public class Route extends RouteBuilder {

    public static final String MEDIA_TYPE_APP_JSON = "application/json";

    @Override
    public void configure() throws Exception {

        restConfiguration().bindingMode(RestBindingMode.json);
        //JacksonDataFormat jsonDataFormat = new JacksonDataFormat(User.class);

        rest("/user")
            .bindingMode(RestBindingMode.json)
            .post()
                .type(User.class)
                .outType(User.class)
                .consumes(MEDIA_TYPE_APP_JSON)
                .produces(MEDIA_TYPE_APP_JSON)
                .to("direct:user-post")
            .get()
                .outType(List.class)
                .produces(MEDIA_TYPE_APP_JSON)
                .to("direct:user-get")
            .get("/{id}")
                .outType(User.class)
                .produces(MEDIA_TYPE_APP_JSON)
                .param()
                    .name("id")
                    .description("The id of the user to get")
                    .dataType("int")
                    .endParam()
                .to("direct:user-get-by-id");

        from("direct:user-post")
                .routeId("save-user-route")
                .log("saving users")
                .to("jpa:" + User.class.getName());

        from("direct:user-get")
                .routeId("list-user-route")
                .log("listing users")
                .to("jpa:" + User.class.getName()+ "?query={{query}}");

        from("direct:user-get-by-id")
                .routeId("get-user-by-id-route")
                .log("listing user by id ${header.id}")
                .toD("jpa:"+User.class.getName()+"?query=select u from users u where u.id= ${header.id}&consumeDelete=false")
                .log("--- select a user ${body} ---")
                .log("--- select a user ${body[0]} ---")
                .log("--- size ${body.size()} ---")
                .setBody().jsonpath("$[0]")
                .marshal().json(JsonLibrary.Jackson)
                .log("--- New body ${body} ---")
                .to("direct:sending-to-kafka");

        from("direct:sending-to-kafka")
            .routeId("send-to-kafka-route")
            .log("sending message.")
            .removeHeaders("*")
            .to("{{kafka.uri}}")
            .removeHeaders("*")
            .setBody(constant("message sent."))
            .log("${body}");
      
    }
}