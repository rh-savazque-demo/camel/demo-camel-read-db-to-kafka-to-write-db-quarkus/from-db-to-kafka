# Read from Data Base H2 and PostgreSQL with Apache Camel

El proposito de este repositorio es tener una referencia de como generar una ruta de prueba con Apace Camel con Quarkus que pueda leer y escribir datos de una base de datos como H2 o PostgreSQL. Para este ejemplo se utilizó el generador de proyectos de Quarkus: https://code.quarkus.io/

Se instalaron las siguientes extensiones:
- Camel REST [camel-quarkus-rest]
- Camel Jackson [camel-quarkus-jackson]
- Camel JPA [camel-quarkus-jpa]
- JDBC Driver - H2 [quarkus-jdbc-h2]
- JDBC Driver - PostgreSQL [quarkus-jdbc-postgresql] (Opcional, solo si se va  aconectar a una base de daatos PostgreSQL)
- Camel Direct [camel-quarkus-direct]

Tecnologias utilizadas (versiones comunitarias):
- Apache Maven 3.9.1
- Java 17 (OpenJDK 17.0.9)
- Camel REST 3.6.0
- Camel Jackson 3.6.0
- Camel JPA 3.6.0
- JDBC Driver - H2 3.6.7
- JDBC Driver - PostgreSQL 3.6.7
- Camel Direct 3.6.0

## Acciones

### Levantar el modo de desarrollo:
```shell script
mvn compile quarkus:dev
```
![Screenshot](assets/readme_images/running.png)

### Leer todos los elementos
```shell script
curl -v -w '\n' http://localhost:8080/user
```

o tambien simplemente
```shell script
curl http://localhost:8080/user
```
![Screenshot](assets/readme_images/read_from_db.png)


### Insertar elementos
```shell script
curl -w '\n' -X POST 'http://localhost:8080/user' -H 'Content-Type: application/json' --data-raw '{ "name": "Tester Tester"}'
```
![Screenshot](assets/readme_images/insert_into_db.png)

## Creando una base de datos
Para el ejercicio se creó una base de datos de PostgreSQL con Podman, con el siguiente comando:
```shell script
podman run -d --name postgresql_database_camel_kafka_quarkus -e POSTGRESQL_USER=user -e POSTGRESQL_PASSWORD=pass -e POSTGRESQL_DATABASE=db -p 5432:5432 registry.redhat.io/rhel9/postgresql-15:1-42.1702482665
```
Y utilizandola con DBeaver
![Screenshot](assets/readme_images/dbeaver.png)


## Fuente

Libro Cloud Native integration with Apache Camel

Repositorio del libro:
    https://github.com/Apress/cloud-native-integration-apache-camel

<br>
<br>
<br>
<br>

# Documentación General de Quarkus

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Dnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Dnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/read-from-db-postgres-camel-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- JDBC Driver - H2 ([guide](https://quarkus.io/guides/datasource)): Connect to the H2 database via JDBC
- Camel JPA ([guide](https://camel.apache.org/camel-quarkus/latest/reference/extensions/jpa.html)): Store and retrieve Java objects from databases using Java Persistence API (JPA)
- Camel Jackson ([guide](https://camel.apache.org/camel-quarkus/latest/reference/extensions/jackson.html)): Marshal POJOs to JSON and back using Jackson
- Camel Rest ([guide](https://camel.apache.org/camel-quarkus/latest/reference/extensions/rest.html)): Expose REST services and their OpenAPI Specification or call external REST services
